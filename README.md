# brawlhalla-api

A Node.js wrapper for the [Brawlhalla API](http://dev.brawlhalla.com).

## Table of contents

- [Installation and Basic Usage](#installation)
- [Static Data](#data)
- [Methods](#methods)

## Change Log

Updating to v4 implements static data (legend info) caching and removes matching on inaccurate legend names.

Updating to v3 once again restructures data. See [Static Data](#data) for details.

Updating to v2 changes how the package is initialized. The API key should now be supplied along with the `require('brawlhalla-api')(YourApiKey)` statement.

## Installation

Install via npm as with any other package:

```js
$ npm install --save brawlhalla-api
```

Within your script:

```js
const bh = require('brawlhalla-api')(YourApiKey);
```

## Data

- bh.legends *(map)* - Uses the legend slug as the key, and object values with the legend's id, name, weapon1 slug, and weapon2 slug.
- bh.legendSummaries *(map)* - Uses the legend slug, legend_id, and bio_name as keys, with basic legend information (no bio information).
- bh.weapons *(object)* - An array of weapon slugs.
- bh.regions *(object)* - An object with server regions as keys and subdomains as values.

## Methods

### .getSteamId(SteamProfileUrl)

```js
bh.getSteamId(SteamProfileUrl).then(function(steamID){

}).catch(function(error){

});
```

*Does not use a Brawlhalla API call.*

### .getBhidBySteamId(steamID)

**Note:** `steamId` must be passed as a string.

```js
bh.getBhidBySteamId(steamID).then(function(bhid){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getBhidBySteamUrl(steamProfileUrl)

```js
bh.getBhidBySteamUrl(steamProfileUrl).then(function(bhid){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getPlayerStats(bhid)

**Note:** `bhid` must be passed as a string.

```js
bh.getPlayerStats(bhid).then(function(playerStats){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getPlayerRanked(bhid)

**Note:** `steamId` must be passed as a string.

```js
bh.getPlayerRanked(bhid).then(function(playerRanked){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getLegendInfo(legend)

`legend` may be the legend id or the legend name.

```js
bh.getLegendInfo(legend).then(function(legendInfo){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call if not previously cached.*

### .getLegendByName(legendName)

Alias for `.getLegendInfo()`.

### .getClanStats(clanId)

**Note:** `steamId` must be passed as a string.

```js
bh.getClanStats(clanId).then(function(clanStats){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .getRankings(options)

```js
bh.getRankings(options).then(function(rankings){

}).catch(function(error){

});
```
The `options` object, along with each of its properties, is optional. Default values are as follows:

```js
options = {
    "bracket": "1v1",
    "region": "all",
    "page": 1,
    "name": null
};
```

*Uses one Brawlhalla API call.*

### .getBhidByName(name)

*Returns exact name matches only.*

```js
bh.getBhidByName(name).then(function(users){

}).catch(function(error){

});
```

*Uses one Brawlhalla API call.*

### .updateLegends()

Refreshes the `.legendSummaries` cache.

```js
bh.updateLegends().then(function(legendSummaries){

}).catch(function(error) {

});
```

*Uses one Brawlhalla API call.*
